﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CartaEticaLoader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string rutaFichero = string.Empty;
        public bool esPro = false;
        public class Fila
        {
            public string DESCRIPCION { get; set; }
            public string TIPO_CONTRATO { get; set; }
            public string ALTA_MANTENIMIENTO { get; set; }
            public string RAZON_SOCIAL { get; set; }
            public string CIF_PROVEEDOR { get; set; }
            public string EMAIL_PROVEEDOR { get; set; }
            public string AREA { get; set; }
            public string SECCION { get; set; }
            public string SECCION_AUX { get; set; }
            public string TIPO_DE_PRODUCTO { get; set; }
            public string TIPO_DE_PRODUCTO_AUX { get; set; }
            public string TIPO_DE_PRODUCTO_AUX_2 { get; set; }
            public string AÑO { get; set; }
            public string ALERTA_FIN_CONTRATO { get; set; }
            public string PERIODICIDAD_ALERTA_FIN_CONTRATO { get; set; }
            public string COD_PROVEEDOR { get; set; }
            public string TRATAMIENTO { get; set; }
            public string SUPECO_WOW { get; set; }
            public string OBSERVACIONES { get; set; }
            public string CONTRATO_RELACIONADO { get; set; }
            public string OBSERVACIONES_SURTIDO { get; set; }
            public string OBSERVACIONES_PROVEEDORES { get; set; }
            public string ROL_CARTA_ETICA { get; set; }
            public string ADCO { get; set; }
            public string NOMBRE_ADCO { get; set; }
            public string NOMBRE_FIRMA_PROVEEDOR { get; set; }
            public string SOCIEDAD_CE { get; set; }
            public string CAPITAL_CE { get; set; }
            public string REGION_MERCANTIL_CE { get; set; }
            public string DOMICILIO_CE { get; set; }
            public string REPRESENTADA_CE { get; set; }
            public string CALIDAD_DE_CE { get; set; }
            public string CIUDAD_CE { get; set; }
            public string FECHA_CE { get; set; }
            public string DIA_CE { get; set; }
            public string MES_CE { get; set; }
            public string AÑO_CE { get; set; }
            public string HECHO_POR_DUPLICADA_EN_CS { get; set; }
            public string ENTIDAD_CS { get; set; }
            public string CAPITAL_CS { get; set; }
            public string DOMICILIO_CS { get; set; }
            public string REPRESENTADA_CS { get; set; }
            public string REPR_CRF_CS { get; set; }
            public string CIUDAD_CS { get; set; }
            public string FECHA_CS { get; set; }
            public string DIA_CS { get; set; }
            public string MES_CS { get; set; }
            public string AÑO_CS { get; set; }

            public Fila()
            {

            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rutaFichero = string.Empty;
                // Create OpenFileDialog
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                // Set filter for file extension and default file extension
                dlg.DefaultExt = ".csv";
                dlg.Filter = "CSV documents (.csv)|*.csv";

                // Display OpenFileDialog by calling ShowDialog method
                Nullable<bool> result = dlg.ShowDialog();

                // Get the selected file name and display in a TextBox
                if (result == true)
                {
                    // Open document
                    cargarFichero(dlg.FileName);
                }
            }
            catch (System.IO.IOException ioe)
            {
                MessageBox.Show("Ha ocurrido un error al leer el fichero. " + ioe.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            DataGrid.ItemsSource = null;
        }

        private void DataGrid_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        internal void cargarFichero(string ruta)
        {
            try
            {
                rutaFichero = ruta;
                // Open document
                string filename = ruta;
                FileNameTextBox.Text = filename;

                ObservableCollection<Fila> filas = new ObservableCollection<Fila>();

                // Leo el CSV
                string[] strCSV = File.ReadAllLines(filename, System.Text.Encoding.UTF8);

                bool esCS = false;
                if (ficheroValido(filename))
                {
                    // Empiezo por la fila uno para saltarme la cabecera
                    for (int i = 1; i < strCSV.Length; i++)
                    {
                        strCSV[i] = strCSV[i].Replace("\"", "");

                        string[] f = strCSV[i].Split(';');
                        Fila _fila = new Fila();

                        _fila = new Fila
                        {
                            DESCRIPCION = f[0],
                            TIPO_CONTRATO = f[1],
                            ALTA_MANTENIMIENTO = f[2],
                            RAZON_SOCIAL = f[3],
                            CIF_PROVEEDOR = f[4],
                            EMAIL_PROVEEDOR = f[5],
                            AREA = f[6],
                            SECCION = f[7],
                            SECCION_AUX = f[8],
                            TIPO_DE_PRODUCTO = f[9],
                            TIPO_DE_PRODUCTO_AUX = f[10],
                            TIPO_DE_PRODUCTO_AUX_2 = f[11],
                            AÑO = f[12],
                            ALERTA_FIN_CONTRATO = f[13],
                            PERIODICIDAD_ALERTA_FIN_CONTRATO = f[14],
                            COD_PROVEEDOR = f[15],
                            TRATAMIENTO = f[16],
                            SUPECO_WOW = f[17],
                            OBSERVACIONES = f[18],
                            CONTRATO_RELACIONADO = f[19],
                            OBSERVACIONES_SURTIDO = f[20],
                            OBSERVACIONES_PROVEEDORES = f[21],
                            ROL_CARTA_ETICA = f[22],
                            ADCO = f[23],
                            NOMBRE_ADCO = f[24],
                            NOMBRE_FIRMA_PROVEEDOR = f[25],
                            SOCIEDAD_CE = f[26],
                            CAPITAL_CE = f[27],
                            REGION_MERCANTIL_CE = f[28],
                            DOMICILIO_CE = f[29],
                            REPRESENTADA_CE = f[30],
                            CALIDAD_DE_CE = f[31],
                            CIUDAD_CE = f[32],
                            FECHA_CE = f[33],
                            DIA_CE = f[34],
                            MES_CE = f[35],
                            AÑO_CE = f[36],
                            ENTIDAD_CS = f[37],
                            CAPITAL_CS = f[38],
                            DOMICILIO_CS = f[39],
                            REPRESENTADA_CS = f[40],
                            REPR_CRF_CS = f[41],
                            CIUDAD_CS = f[42],
                            FECHA_CS = f[43],
                            DIA_CS = f[44],
                            MES_CS = f[45],
                            AÑO_CS = f[46],
                            HECHO_POR_DUPLICADA_EN_CS = f[47]
                        };

                        filas.Add(_fila);
                        
                    }

                    DataGrid.ItemsSource = filas;
                }
                else
                {
                    MessageBox.Show("El fichero no es válido. Por favor, revise el número de filas.");
                }
            }
            catch (System.IO.IOException ioe)
            {
                MessageBox.Show("Ha ocurrido un error al leer el fichero. " + ioe.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            txtMensaje.Text = "";


            string usuario = "Carta Etica TK";
            string clave = "Carrefour16";
            string idFlujo = (esPro ? "101" : "149");

            string campos = string.Empty;
            string razonSocial = string.Empty;

            string filename = rutaFichero;
            FileNameTextBox.Text = filename;

            if (ficheroValido(filename))
            {
                
                Fila f = new Fila();
                //FilaCE fCE = new FilaCE();
                txtMensaje.Text = "Iniciando el procesado del fichero." + Environment.NewLine;
                DoEvents();
                try
                {
                    string[] strCSV = File.ReadAllLines(filename, System.Text.Encoding.UTF8);
                    signesDemos.WorkFlow ws = new signesDemos.WorkFlow();

                    if (esPro)
                        ws.Url = "https://carrefour-bizlayer.com/signespro/workflow.asmx";
                    
                    // Recorremos las filas
                    // Restamos 1 fila por que por defecto la aplicación siempre deja la última fila en blanco.
                    for (int i = 0; i < DataGrid.Items.Count; i++)
                    {
                        try
                        {
                            f = (Fila)DataGrid.Items.GetItemAt(i);
                        }
                        catch (Exception exx)
                        {
                            continue;
                        }

                        bool esCS = esCartaSocial(f);
                        string idFormulario = string.Empty;

                        if (esPro)
                            idFormulario = (esCS ? "135" : "135");
                        else
                            idFormulario = (esCS ? "1097" : "1095");

                        string descripcion = f.DESCRIPCION;
                        //string descripcion = (esCS ? fCS.DESCRIPCION : fCE.DESCRIPCION);

                        txtMensaje.Text += Environment.NewLine + (i + 1) + ". Procesando '" + descripcion + "'.";
                        DoEvents();

                        // Generamos los campos
                        campos = obtenerCampos(f);

                        string idFicha = ws.iniciarFlujo(usuario, clave, idFlujo, descripcion, campos);

                        if (!idFicha.Equals("-1"))
                        {
                            txtMensaje.Text += Environment.NewLine + "  INICIADO - ";
                            DoEvents();
                            idFicha = idFicha.Split('#')[0];

                            // Subimos el formulario
                            string resp = ws.subirFicheroFormulario(usuario, clave, idFlujo, idFicha, idFormulario);

                            if (!resp.Equals("-1")){
                                txtMensaje.Text += " FICHERO CREADO - PROCESADO";

                                // DANI ESTA ES LA LINEA.
                                //resp = ws.asignarPaso(usuario, clave, idFlujo, idFicha, 3, f.EMAIL_PROVEEDOR);
                            }

                            else
                                txtMensaje.Text += " -- ERROR AL CREAR EL FICHERO PARA '" + descripcion + "'.";
                            DoEvents();
                        }
                        else
                        {
                            txtMensaje.Text += " -- ERROR AL INICIAR EL PROCESO '" + descripcion + "'.";
                            DoEvents();
                        }
                    }

                    txtMensaje.Text += Environment.NewLine + Environment.NewLine + "Fichero procesado.";
                    DoEvents();
                    MessageBox.Show("Proceso finalizado.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private string obtenerCampos(Fila f)
        {
            string campos = string.Empty;
            try
            {
                campos = "<campos><campo>";

                campos += "<TIPO_CONTRATO>" +                       f.TIPO_CONTRATO                     + "</TIPO_CONTRATO>";
                campos += "<ALTA_MANTENIMIENTO>" +                  f.ALTA_MANTENIMIENTO                + "</ALTA_MANTENIMIENTO>";
                campos += "<RAZON_SOCIAL>" +                        f.RAZON_SOCIAL                      + "</RAZON_SOCIAL>";
                campos += "<CIF_PROVEEDOR>" +                       f.CIF_PROVEEDOR                     + "</CIF_PROVEEDOR>";
                campos += "<EMAIL_PROVEEDOR>" +                     f.EMAIL_PROVEEDOR                   + "</EMAIL_PROVEEDOR>";
                campos += "<AREA>" +                                f.AREA                              + "</AREA>";
                campos += "<SECCION>" +                             f.SECCION                           + "</SECCION>";
                campos += "<SECCION_AUX>" +                         f.SECCION_AUX                       + "</SECCION_AUX>";
                campos += "<TIPO_DE_PRODUCTO>" +                    f.TIPO_DE_PRODUCTO                  + "</TIPO_DE_PRODUCTO>";
                campos += "<TIPO_DE_PRODUCTO_AUX>" +                f.TIPO_DE_PRODUCTO_AUX              + "</TIPO_DE_PRODUCTO_AUX>";
                campos += "<TIPO_DE_PRODUCTO_AUX_2>" +              f.TIPO_DE_PRODUCTO_AUX_2            + "</TIPO_DE_PRODUCTO_AUX_2>";
                campos += "<AÑO>" +                                 f.AÑO                               + "</AÑO>";
                campos += "<ALERTA_FIN_CONTRATO>" +                 f.ALERTA_FIN_CONTRATO               + "</ALERTA_FIN_CONTRATO>";
                campos += "<PERIODICIDAD_ALERTA_FIN_CONTRATO>" +    f.PERIODICIDAD_ALERTA_FIN_CONTRATO  + "</PERIODICIDAD_ALERTA_FIN_CONTRATO>";
                campos += "<COD_PROVEEDOR>" +                       f.COD_PROVEEDOR                     + "</COD_PROVEEDOR>";
                campos += "<TRATAMIENTO>" +                         f.TRATAMIENTO                       + "</TRATAMIENTO>";
                campos += "<SUPECO_WOW>" +                          f.SUPECO_WOW                        + "</SUPECO_WOW>";
                campos += "<OBSERVACIONES>" +                       f.OBSERVACIONES                     + "</OBSERVACIONES>";
                campos += "<CONTRATO_RELACIONADO>" +                f.CONTRATO_RELACIONADO              + "</CONTRATO_RELACIONADO>";
                campos += "<OBSERVACIONES_SURTIDO>" +               f.OBSERVACIONES_SURTIDO             + "</OBSERVACIONES_SURTIDO>";
                campos += "<OBSERVACIONES_PROVEEDORES>" +           f.OBSERVACIONES_PROVEEDORES         + "</OBSERVACIONES_PROVEEDORES>";
                campos += "<ROL_CARTA_ETICA>" +                     f.ROL_CARTA_ETICA                   + "</ROL_CARTA_ETICA>";
                campos += "<ADCO>" +                                f.ADCO                              + "</ADCO>";
                campos += "<NOMBRE_ADCO>" +                         f.NOMBRE_ADCO                       + "</NOMBRE_ADCO>";
                campos += "<NOMBRE_FIRMA_PROVEEDOR>" +              f.NOMBRE_FIRMA_PROVEEDOR            + "</NOMBRE_FIRMA_PROVEEDOR>";
                campos += "<SOCIEDAD_CE>" +                         f.SOCIEDAD_CE                       + "</SOCIEDAD_CE>";
                campos += "<CAPITAL_CE>" +                          f.CAPITAL_CE                        + "</CAPITAL_CE>";
                campos += "<REGION_MERCANTIL_CE>" +                 f.REGION_MERCANTIL_CE               + "</REGION_MERCANTIL_CE>";
                campos += "<DOMICILIO_CE>" +                        f.DOMICILIO_CE                      + "</DOMICILIO_CE>";
                campos += "<REPRESENTADA_CE>" +                     f.REPRESENTADA_CE                   + "</REPRESENTADA_CE>";
                campos += "<CALIDAD_DE_CE>" +                       f.CALIDAD_DE_CE                     + "</CALIDAD_DE_CE>";
                campos += "<CIUDAD_CE>" +                           f.CIUDAD_CE                         + "</CIUDAD_CE>";
                campos += "<FECHA_CE>" +                            f.FECHA_CE                          + "</FECHA_CE>";
                campos += "<DIA_CE>" +                              f.DIA_CE                            + "</DIA_CE>";
                campos += "<MES_CE>" +                              f.MES_CE                            + "</MES_CE>";
                campos += "<AÑO_CE>" +                              f.AÑO_CE                            + "</AÑO_CE>";
                campos += "<HECHO_POR_DUPLICADA_EN_CS>" +           f.HECHO_POR_DUPLICADA_EN_CS         + "</HECHO_POR_DUPLICADA_EN_CS>";
                campos += "<ENTIDAD_CS>" +                          f.ENTIDAD_CS                        + "</ENTIDAD_CS>";
                campos += "<CAPITAL_CS>" +                          f.CAPITAL_CS                        + "</CAPITAL_CS>";
                campos += "<DOMICILIO_CS>" +                        f.DOMICILIO_CS                      + "</DOMICILIO_CS>";
                campos += "<REPRESENTADA_CS>" +                     f.REPRESENTADA_CS                   + "</REPRESENTADA_CS>";
                campos += "<REPR_CRF_CS>" +                         f.REPR_CRF_CS                       + "</REPR_CRF_CS>";
                campos += "<CIUDAD_CS>" +                           f.CIUDAD_CS                         + "</CIUDAD_CS>";
                campos += "<FECHA_CS>" +                            f.FECHA_CS                          + "</FECHA_CS>";
                campos += "<DIA_CS>" +                              f.DIA_CS                            + "</DIA_CS>";
                campos += "<MES_CS>" +                              f.MES_CS                            + "</MES_CS>";
                campos += "<AÑO_CS>" +                              f.AÑO_CS                            + "</AÑO_CS>";

                campos += "</campo></campos>";

                return campos;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return "";
            }
        }

        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

        private bool ficheroValido(string filename)
        {
            try
            {
                // Para ser válido debe contener:
                // MÍNIMO 2 FILAS: Cabecera + Una fila de datos
                // SIEMPRE 48 COLUMNAS, Tanto si es CE como CS.

                string[] strCSV = File.ReadAllLines(filename, System.Text.Encoding.Default);

                return strCSV.Length > 1 && (strCSV[0].Split(';').Length == 48);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private bool esCartaSocial(Fila f)
        {
            try
            {
                //No hace falta comprobar que es válido porqué solo entrará en este método si lo es.
                return f.TIPO_CONTRATO.Equals("Carta Ética y Social");
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
